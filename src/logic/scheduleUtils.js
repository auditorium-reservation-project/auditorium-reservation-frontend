/**
 *
 * @param today a string in ISO8601 format
 * @return {Date} a date object of monday on this week
 */
export function getMonday(_today = new Date()) {
    // today = new Date(today)
    // let day = today.getDay(),
    //     diff = today.getDate() - day + (day === 0 ? -6:1) // adjust when day is sunday
    // let monday = new Date(today.setDate(diff))
    // monday.setMilliseconds(monday.getMilliseconds() - (monday.getTimezoneOffset() * 60 * 1000)) // корректируем дату на разницу с UTC
    // return monday
    let today = new Date(_today)
    var day = today.getDay() || 7;              // Get current day number, converting Sun. to 7
    if( day !== 1 ) {                           // Only manipulate the date if it isn't Mon.
        today.setHours(-24 * (day - 1));        // Set the hours to day number minus 1
    }                                           //   multiplied by negative 24
    return today
}
/**
 *
 * @param today a string in ISO8601 format
 * @return {Date} a date object of saturday on this week
 */
export function getSaturday(today = new Date()) {
    // today = new Date(today)
    // let day = today.getDay(),
    //     diff = today.getDate() - day + (day === 0 ? -1:6)
    // let saturday = new Date(today.setDate(diff))
    // saturday.setMilliseconds(saturday.getMilliseconds() - (saturday.getTimezoneOffset() * 60 * 1000))
    // return saturday
    return addDays(getMonday(today), 5)
}

export function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
  
/**
 *
 * @param today a string in ISO8601 format
 * @return {Date} a date object of sunday on this week
 */
export function getSunday(today) {
    today = new Date(today)
    let day = today.getDay(),
        diff = today.getDate() - day + (day === 0 ? 0:7) - 1 // adjust when day is not Sunday
    let sunday = new Date(today.setDate(diff))
    sunday.setMilliseconds(sunday.getMilliseconds() - (sunday.getTimezoneOffset() * 60 * 1000)) // корректируем дату на разницу с UTC
    return sunday    
}


/**
 * Gets array of timeslots and returns it in time like "HH:MM" instead of ISOstring
 * @param timeSlotsInISO8601 timeslots array
 * @return {*} repaired timeslots array
 */
export function modifyTimeSlots(timeSlotsInISO8601) {
    return timeSlotsInISO8601.map((item) => {
        let startTime = new Date(item.startTime)
        let endTime = new Date(item.endTime)
        return {
            number: item.number,
            startTime: `${startTime.getHours()}:${startTime.getMinutes() === 0 ? "00" : startTime.getMinutes()}`,
            endTime: `${endTime.getHours()}:${endTime.getMinutes() === 0 ? "00" : endTime.getMinutes()}`,
            name: `${startTime.getHours()}:${startTime.getMinutes() === 0 ? "00" : startTime.getMinutes()} - ${endTime.getHours()}:${endTime.getMinutes() === 0 ? "00" : endTime.getMinutes()}`
        }
    })
}