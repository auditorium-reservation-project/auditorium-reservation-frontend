import Roles from '../api/Roles'

function PageLink(id, name, ref = '', callback = () => {}, allowedRoles = []){
    return {
        id: id,
        name: name,
        ref: ref,        
        callback: callback,
        roles: allowedRoles
    }
}

const pages = [
    new PageLink(0, "Main page", '/'),
    new PageLink(1, "Schedule", '/schedule'),
    new PageLink(2, "Reservations", '/reservations', () => {}, [Roles.student, Roles.staff, Roles.admin]),
    new PageLink(3, "Education", '/education', () => {}, [Roles.staff, Roles.admin])
]

const profileOptions = [
    new PageLink(1, 'My Profle', '/profile'),
    new PageLink(2, "Staff Only", '/schedule/admin-tools', () => {}, [Roles.staff, Roles.admin]),
]

export {pages as navBarLinks, profileOptions}