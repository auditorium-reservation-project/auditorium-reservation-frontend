import React from 'react'
import {Route, Routes, useSearchParams} from 'react-router-dom'
import Layout from './components/Util/Layout'
import ProfilePage from './pages/ProfilePage'
import RegistrationPage from './pages/RegistrationPage'
import MainPage from './pages/MainPage'
import SchedulePage from './pages/SchedulePage'
// import SubjectsPage from './pages/SubjectsPage'
// import ProfessorsPage from './pages/ProfessorsPage'
// import AuditoriaPage from './pages/AuditoriaPage'
// import BuildingsPage from './pages/BuildingsPage'
// import RoleApplicationsPage from './pages/RoleApplicationsPage'
import LogInPage from './pages/LogInPage'
import UnauthorizedPage from './pages/UnauthorizedPage'
import NotFoundPage from './pages/NotFoundPage'
import RequireAuth from './components/Util/RequireAuth'
import Roles from './api/Roles'
import ScheduleQueryPage from './pages/ScheduleQueryPage'
import ScheduleEditPage from './pages/ScheduleEditPage'
import ScheduleAdminPage from './pages/ScheduleAdminPage'
import ScheduleReservationsPage from './pages/ScheduleReservationsPage'
import ScheduleReservationsQueryPage from './pages/ScheduleReservationsQueryPage'
import RoleApplicationPage from './pages/RoleApplicationPage'
import ReservationApprovalPage from './pages/ReservationApprovalPage'
// import UsersPage from './pages/UsersPage'


function App() {
    const [searchParams, setSearchParams] = useSearchParams()

    return (
        <Routes>
            <Route path="/" element={<Layout/>}>

                <Route path="unauthorized" element={<UnauthorizedPage />} />

                  <Route index element={<MainPage />} />
                  <Route path="main" element={<MainPage />} />
                  <Route path="login" element={<LogInPage />} />
                  <Route path="registration" element={<RegistrationPage />} />

                  <Route path="schedule">
                    <Route index element={<SchedulePage />} />
                    <Route path="query" element={<ScheduleQueryPage />} />
                    <Route element ={<RequireAuth allowedRoles={[Roles.staff, Roles.admin]}/>} >
                      <Route path="edit" element={<ScheduleEditPage />}/>
                      <Route path="admin-tools" element={<ScheduleAdminPage />}/>
                    </Route>
                  </Route>

                  <Route path='reservations' element ={<RequireAuth allowedRoles={[Roles.student, Roles.staff, Roles.admin]}/>} >
                    <Route index element={<ScheduleReservationsPage />}/>
                    <Route path="query" element={<ScheduleReservationsQueryPage />} />

                    <Route element ={<RequireAuth allowedRoles={[Roles.staff, Roles.admin]}/>} >
                      <Route path="consider" element={<ReservationApprovalPage />} />
                    </Route>
                  </Route>

                  {/* <Route element ={<RequireAuth allowedRoles={[Roles.staff]}/>} >
                    <Route path="accounts">
                      <Route path="applications" element={<RoleApplicationsPage />} />
                      <Route path="users" element={<UsersPage />} />
                    </Route>                    
                  </Route>
                  <Route path="education" element ={<RequireAuth allowedRoles={[Roles.staff]}/>} >
                      <Route path="subjects" element={<SubjectsPage />} />
                      <Route path="professors" element={<ProfessorsPage />} />
                      <Route path="groups" element={<GroupsPage />} />
                  </Route> */}

                    <Route element={<RequireAuth/>}>
                        <Route path="profile">
                          <Route index element={<ProfilePage />} />
                          <Route path="roles" element={<RoleApplicationPage />} />
                        </Route>
                    </Route>

                    <Route path="*" element={<NotFoundPage/>}/>
                </Route>
        </Routes>)
}


export default App


