import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import reportWebVitals from './reportWebVitals'
import CssBaseline from '@mui/material/CssBaseline'
import { ThemeProvider } from '@mui/material/styles'
import theme from './theme'
import { BrowserRouter } from 'react-router-dom'
import { IdentityProvider } from './context/IdentityProvider'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <BrowserRouter>
    <ThemeProvider theme={theme}>

      <CssBaseline />
      
      <IdentityProvider>
        <App />
      </IdentityProvider>
      
    </ThemeProvider>
  </BrowserRouter>
)
