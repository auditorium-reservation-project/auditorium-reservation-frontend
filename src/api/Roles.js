const Roles = {
    default: "Default",
    staff: "Staff",
    student: "Student",
    professor: "Professor",
    admin: "Admin"
}

export default Roles