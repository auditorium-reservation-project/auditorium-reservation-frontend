import axios from "axios"

const BASE_URL = process.env.REACT_APP_API_URL

const instance = axios.create({
    baseURL: BASE_URL,
    withCredentials: true,
    headers: {
        'Content-Type': 'application/json',
        'Accept': '*/*',
    }
})

instance.interceptors.response.use(
    response => response,
    async(error) => {
        if(error?.response?.status === 401){
            console.warn("Unauthorized, add login navigation through routing")
        }
        
        return Promise.reject(error)
    }
)

export default instance
// export const axiosPrivate = axios.create({
//     baseURL: BASE_URL,
//     withCredentials: false,
//     headers: {
//         'Content-Type': 'application/json',
//         'Accept': '*/*',
//     }
// })