import React, { useEffect, useState } from 'react'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import { InputLabel } from '@mui/material'

const OtherSelect = ({onChange = () => {}, items = [], localizations = [], label, initializeWith = null}) => {
    const [selected, setSelected] = useState(initializeWith != null && items?.find(val => val===initializeWith) ? initializeWith :  items?.[0])

    const handleChange = (event) => {
        setSelected(event.target.value)
    }

    useEffect(() => {
        onChange(selected)
    }, [selected])

    useEffect(() => {
        setSelected(initializeWith != null && items.find(val => val===initializeWith) ? initializeWith : items?.[0])
    }, [items])

    return ( 
        <FormControl fullWidth>
            { label && <InputLabel id={label}> {label} </InputLabel> }
            <Select 
                defaultValue={items?.[0]}
                value={items?.find(x => x === selected) != null ? selected : items?.[0]}
                onChange={handleChange}
                displayEmpty
                label={label}
                labelId={label}
                id={label}
                variant='outlined'
                color='btnSecondary'
                sx={{ fontSize: "1rem" }}

                {...(label && {label: label, labelId: label})}
            >
                {
                    items?.map( (item, index) =>
                        <MenuItem value={item} key={index}> { localizations?.[index] || item } </MenuItem>
                    )
                }
            </Select>
        </FormControl>
    )

}

export default OtherSelect