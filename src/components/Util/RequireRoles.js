import React from 'react'
import useIdentity from '../../hooks/useIdentity'

const RequireRoles = ({allowedRoles = [], children}) => {
    const { user } = useIdentity()

    return (
    <>
        {
            user?.roles?.find(role => allowedRoles?.includes(role)) && children
        }
    </>
    )
}

export default RequireRoles