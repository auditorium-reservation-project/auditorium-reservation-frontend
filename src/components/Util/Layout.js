import React from 'react'
import Footer from './Footer'
import AppBar from '../AppBar/AppBar'
import { Outlet } from 'react-router-dom'
import { AlertHandler } from '../../context/AlertHandler'

function Layout(){    
    return (
        <AlertHandler>
            <AppBar />
            <Outlet />
            <Footer />
        </AlertHandler>
    )
}

export default Layout