import { Navigate, Outlet } from 'react-router-dom'
import useIdentity from '../../hooks/useIdentity'
import { useMemo } from 'react'

const RequireAuth = ({ allowedRoles="all" }) => {
    const { user } = useIdentity()
    
    const isAllowed = useMemo(() => (
        allowedRoles === "all" && user !== null) 
        || user?.roles?.find(role => allowedRoles?.includes(role))
    )
    //if all allowed roles -> just requires a log-in
    //if a user satisfies a role -> outlet
    //if does not, but still logged in -> /unauthorized
    //if does not and not logged in -> /login
    return(
        isAllowed
            ? <Outlet />
            : user !== null
                ? <Navigate to='/unauthorized' replace />
                : <Navigate to='/login' replace/>
    )
}

export default RequireAuth