import React from 'react'
import { Typography, Link } from '@mui/material'

function Footer() {
    return (
        <footer style={{ width: '100%', height: 'auto', paddingTop: '1rem', paddingBottom: '1rem' }}>
            <Typography variant="body2" color="text.secondary" align="center" sx={{ mx: 4 }}>
                {' '}
                <Link color="inherit" href="https://www.ravbug.com/bsod/bsod7/">
                    Auditorium Reservation
                </Link>
                    {/* {' '}
                    {new Date().getFullYear()}
                    {'.'} */}
            </Typography>
        </footer>
    )
}

export default Footer