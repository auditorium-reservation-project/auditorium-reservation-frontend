import React, { useState, useRef, useEffect } from 'react'
import { Grid, IconButton, Select, TextField, Typography } from '@mui/material'
import EditIcon from '@mui/icons-material/Edit'
import CheckIcon from '@mui/icons-material/Check'
import CancelIcon from '@mui/icons-material/Cancel'
import useAlerts from '../../hooks/useAlerts'
import OtherSelect from './OtherSelect'

const AttributeSelect = ({title, value, items = [], localizations =[], hint, onSubmit = () => {}, editable = true}) => {
    const [editing, setEditing] = useState(false)
    const [newValue, setNewValue] = useState(items?.[0] || '')
    const { alert } = useAlerts()

    const handleChange = (value) => {
        setNewValue(value)
    }

    const handleSubmit = () => {
        if(!newValue){
            alert(hint)
            return
        }
        setEditing(false)
        onSubmit(newValue)
    }

    const handleCancel = () => {
        setEditing(false)
    }

    return (
    <>
        <Grid item xs={4} sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
            {title}:
        </Grid>
        <Grid item xs={5} sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
            { !editing ?
                <Typography sx={{ my: 2 }}>{value}</Typography>
                :
                <OtherSelect items={items} localizations={localizations} onChange={handleChange} initializeWith={value}/>
            }
        </Grid>
        <Grid item xs={3} sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
        { editable &&
            (!editing ?
                <IconButton onClick={() => setEditing(true)}>
                    <EditIcon />
                </IconButton>
            : 
            <>
                <IconButton onClick={handleSubmit}>
                    <CheckIcon />
                </IconButton>
                <IconButton onClick={handleCancel}>
                    <CancelIcon />
                </IconButton>
            </>)
        }
        </Grid>

    </>
    )
}

export default AttributeSelect