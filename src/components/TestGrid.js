import React from 'react';
import { Grid, Paper, styled } from '@mui/material';

const GridCell = styled(Paper)(({ theme }) => ({
  border: '1px solid #e0e0e0',
  minHeight: 100,
}));

const ColumnHeader = styled(Paper)(({ theme }) => ({
  backgroundColor: '#f0f0f0',
  textAlign: 'center',
  fontWeight: 'bold',
  border: '1px solid #e0e0e0'
}));

const daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function GridComponent() {
  return (
    <Grid container>
      {daysOfWeek.map((day, index) => (
        <Grid item xs={1} key={index}>
          <ColumnHeader>
            {day}
          </ColumnHeader>
        </Grid>
      ))}

      {/* Render the grid cells */}
      {Array.from({ length: 6 }).map((_, rowIndex) => (
        <Grid container item key={rowIndex}>
          {Array.from({ length: 7 }).map((_, colIndex) => (
            <Grid item xs={1} key={colIndex}>
              <GridCell></GridCell>
            </Grid>
          ))}
        </Grid>
      ))}
    </Grid>
  );
}

export default GridComponent
