import React from 'react'
import { Box, IconButton, Typography, Menu,  Avatar, Tooltip, MenuItem } from '@mui/material'
import { useNavigate } from 'react-router'
import useIdentity from '../../hooks/useIdentity'
import theme from '../../theme'

function ProfilePlate({profileOptions, handleCloseUserMenu, anchorElUser, handleOpenUserMenu, userAuth}){
  const navigate = useNavigate()
  const { logout } = useIdentity()
  return (
      <>
      <Typography sx={{color: theme.text.primary.main}}>{userAuth?.firstName}</Typography>
      <Box sx={{ flexGrow: 0, ml: 1 }}>
        <Tooltip title="My Profile">
          <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
            <Avatar src={userAuth?.avatarUrl}/>
          </IconButton>
        </Tooltip>
        <Menu sx={{ mt: '45px' }}
          id="menu-appbar"
          anchorEl={anchorElUser}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          keepMounted
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={Boolean(anchorElUser)}
          onClose={handleCloseUserMenu}
        >
          {profileOptions.map((setting) => (
            (setting?.roles?.length <= 0 ||
            userAuth?.roles?.find(role => setting?.roles?.includes(role))) &&
            <MenuItem 
              key={setting.id}
              // to={setting.ref}
              onClick={(event) => {
                    setting.callback()
                    handleCloseUserMenu(event)
                    if(setting?.ref && setting?.ref !== ""){
                      navigate(setting.ref)
                    }
                  }
                }>
              <Typography textAlign="center">{setting.name}</Typography>
            </MenuItem>              
          ))}
          <MenuItem 
              key={10}
              onClick={(event) => {
                handleCloseUserMenu(event)
                logout()
              }
            }>
              <Typography textAlign="center">Logout</Typography>
            </MenuItem>
        </Menu>
      </Box>
      </>
    )
}

export default ProfilePlate