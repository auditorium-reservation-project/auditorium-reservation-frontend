import React, { useEffect, useState } from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import { AppBar as AppBarMui, Box, Toolbar, IconButton, Typography, Menu, Container, MenuItem } from '@mui/material'
import MenuIcon from '@mui/icons-material/Menu'
import AppIcon from '@mui/icons-material/AlarmOff'
import ProfilePlate from './ProfilePlate'
import LoginButton from './LoginButton'
import theme from '../../theme'
import NavButton from './NavButton'
import { navBarLinks, profileOptions } from '../../logic/appBarLinks'
import useIdentity from '../../hooks/useIdentity'

const getCurrentActivePage = (currentPath) => {
    const found = navBarLinks.find(x => x.ref === '/' + currentPath?.split('/')?.[1])
    return found ? found.id : -1
}

function AppBar() {
  const location = useLocation()
  const [anchorElNav, setAnchorElNav] = useState(null)
  const [anchorElUser, setAnchorElUser] = useState(null)
  const [activePage, setActivePage] = useState(getCurrentActivePage(location.pathname))
  const { user } = useIdentity()
  
  useEffect(() => {
    setActivePage(getCurrentActivePage(location.pathname))
  },[location])

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget)
  }
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget)
  }

  const handleCloseNavMenu = () => {
    setAnchorElNav(null)
  }

  const handleCloseUserMenu = () => {
    setAnchorElUser(null)
  }

  const handlePageButtonPress = (id) => {
    setActivePage(id)
    handleCloseNavMenu()
  }

  return (
    <AppBarMui position="sticky" elevation={0} sx={{boxShadow: 0, width:'100%', backgroundColor: theme.palette.secondary.main}}>
      <Container maxWidth='none'>
        <Toolbar disableGutters>
          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              { navBarLinks.map(page => ( 
                (page?.roles?.length <= 0 ||
                user?.roles?.find(role => page?.roles?.includes(role))) &&
                <MenuItem key={page.id} component={NavLink} to={page.ref} onClick={() => handlePageButtonPress(page.id)} >
                  <Typography textAlign="center">{page.name}</Typography>                  
                </MenuItem>
              )) }
            </Menu>
          </Box>
          <AppIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} />
          <Typography
            variant="h5"
            noWrap
            component="a"
            href=""
            sx={{
              mr: 2,
              display: { xs: 'flex', md: 'none' },
              flexGrow: 1,
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            Main Page
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {navBarLinks.map((page) => ( 
                (page?.roles?.length <= 0 ||
                user?.roles?.find(role => page?.roles?.includes(role))) &&
                <NavButton key={page.id} page={page} isActivePage={activePage === page.id} onClickHandler={() => handlePageButtonPress(page.id)} location={location}/>
              )
            )}
          </Box>
          {user ? 
            <ProfilePlate 
              profileOptions={profileOptions} 
              handleCloseUserMenu={handleCloseUserMenu} 
              anchorElUser={anchorElUser} 
              handleOpenUserMenu={handleOpenUserMenu} 
              userAuth={user} />
            : 
            <LoginButton />
        }
        </Toolbar>
      </Container>
    </AppBarMui>
  )
}
export default AppBar