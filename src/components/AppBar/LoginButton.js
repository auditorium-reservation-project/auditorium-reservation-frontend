import React from 'react'
import { Button } from '@mui/material'
import { NavLink, useLocation } from 'react-router-dom'
import theme from '../../theme'

function LoginButton(){
    const location = useLocation()
    return(
        <Button                  
            draggable='false'
            key='toLogin'
            component={NavLink}
            to='/login'
            state={location.pathname !== 'unauthorized' && {from: location}}
            disableElevation
            color='btnSecondary'
            variant='contained'
            sx={{ 
                my: 0,
                py: 0.2,
                px: 3,
                color: theme.palette.text.secondary,
                display: 'block',
                borderRadius: "8px" ,
                textTransform: 'none',
                fontSize: '1rem'                    
            }}>
            Войти
        </Button>
    )
}

export default LoginButton