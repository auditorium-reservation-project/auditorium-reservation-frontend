import React from 'react'
import { Button } from '@mui/material'
import { NavLink } from 'react-router-dom'
import theme from '../../theme'

function NavButton({page, isActivePage, onClickHandler, location}){
    return(
        <Button
            draggable='false'            
            onClick={onClickHandler}
            component={NavLink}
            to={page.ref}
            state={{from: location}}             
            sx={{ 
            my: 0,
            py: 0.2,
            px: 3,
            mx: 0.5,
            color: isActivePage ? theme.text.primary.light : theme.text.primary.main, 
            display: 'block', 
            borderRadius: "8px" ,
            textTransform: 'none',
            fontSize: '1rem'                    
            }}
            {...(isActivePage && 
            {
                color: 'selected', 
                variant: 'contained', 
                disableElevation: true
            }
            )}>

        {page.name}
        </Button>
    )
}

export default NavButton