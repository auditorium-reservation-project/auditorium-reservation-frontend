import React, { useState, useRef, useEffect } from 'react'
import { Grid, IconButton, TextField, Typography } from '@mui/material'
import EditIcon from '@mui/icons-material/Edit'
import CheckIcon from '@mui/icons-material/Check'
import CancelIcon from '@mui/icons-material/Cancel'
import useAlerts from '../../hooks/useAlerts'

const ProfileAttribute = ({title, value, regex, hint, autoComplete, onSubmit = () => {}, editable = true}) => {
    const [editing, setEditing] = useState(false)
    const [newValue, setNewValue] = useState(value || '')
    const [isValid, setValid] = useState(true)
    const firstMount = useRef(true)
    const initializeValue = useRef(true)
    const { alert } = useAlerts()
    
    useEffect(() => {
        if(value){
            setNewValue(value)
        }
    }, [value])

    useEffect(() => {
        if(firstMount.current){
            firstMount.current = false
            return
        }
        if(regex){
            setValid(regex.test(newValue))
        }
    }, [newValue])

    const handleSubmit = () => {
        if(!regex.test(newValue)){
            alert(hint)
            return
        }
        setEditing(false)
        setValid(true)
        initializeValue.current = true
        onSubmit(newValue)
    }

    const handleCancel = () => {
        setEditing(false)
        setValid(true)
        initializeValue.current = true
    }

    const getValue = () => {
        if(initializeValue.current && value){
            initializeValue.current = false
            return value
        }
        return newValue
    }

    return (
    <>
        <Grid item xs={4} sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
            {title}:
        </Grid>
        <Grid item xs={5} sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
            { !editing ?
                <Typography sx={{ my: 2 }}>{value}</Typography>
                :
                <TextField
                    required
                    fullWidth
                    id={title}
                    name={title}
                    autoComplete={autoComplete}            
                    onChange={event => setNewValue(event?.target?.value)}
                    error={!isValid}
                    helperText={!isValid && hint}
                    value={getValue()}
                />
            }
        </Grid>
        <Grid item xs={3} sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
        { editable &&
            (!editing ?
                <IconButton onClick={() => setEditing(true)}>
                    <EditIcon />
                </IconButton>
            : 
            <>
                <IconButton onClick={handleSubmit}>
                    <CheckIcon />
                </IconButton>
                <IconButton onClick={handleCancel}>
                    <CancelIcon />
                </IconButton>
            </>)
        }
        </Grid>

    </>
    )
}

export default ProfileAttribute