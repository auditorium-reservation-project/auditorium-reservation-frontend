import React from 'react'
import { Card, CardActionArea, CardContent, Typography } from '@mui/material'
import { NavLink } from 'react-router-dom'

const LinkCard = ({title, desc, href, location, onClick = () => {}}) => {

    return (
        <Card variant='outlined' sx={{ my:1 }}>
            <CardActionArea onClick={onClick} 
                {...(href && {
                    component: NavLink, 
                    to: href, 
                    replace: true, 
                    state: {from: location}
                })}>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {desc}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    )
}

export default LinkCard