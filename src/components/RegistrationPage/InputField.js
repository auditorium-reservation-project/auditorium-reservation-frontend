import React, { useState, useEffect, useRef } from 'react'
import { TextField } from '@mui/material'

const InputField = ({label, name, autoComplete, hint, regex, props}) => {
    const [value, setValue] = useState("")
    const [isValid, setValid] = useState(true)
    const firstMount = useRef(true)
    

    useEffect(() => {
        if(firstMount.current){
            firstMount.current = false
            return
        }
        setValid(regex.test(value))
    }, [value])

    return (
        <TextField
            required
            fullWidth
            label={label}
            id={name}
            name={name}
            autoComplete={autoComplete}            
            onChange={event => setValue(event?.target?.value)}
            error={!isValid}
            helperText={!isValid && hint}
            {...props}
        />
    )
}

export default InputField