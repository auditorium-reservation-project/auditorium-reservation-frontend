import { useEffect, useState } from 'react'
import ToggleButton from '@mui/material/ToggleButton'
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup'

const TypeSelector = ({ states=[{ name: "state0", label: "Type Selector Option" }], onChange}) => {
    const [selected, setSelected] = useState(states.length > 0 ? states[0]?.name : null)

    const handleChange = (event, newState) => {
        // event.preventDefault()
        if(newState !== null){
            setSelected(newState)
            onChange(newState)
        }
    }
    
    useEffect(() => {
        onChange(selected)
    }, [])

    if(states.length <= 0) {
        return (<></>)
    }
    
    return (
        <ToggleButtonGroup            
            color="primary"
            value={selected}
            onChange={handleChange}
            exclusive
            fullWidth
        >
        { states.map(state => (
            <ToggleButton value={state.name} key={state.name}>{state.label}</ToggleButton>
            )
        )}
        </ToggleButtonGroup>
    )
}

export default TypeSelector