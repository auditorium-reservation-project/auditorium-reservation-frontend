import React, { useEffect } from 'react'
import TableContainer from '@mui/material/TableContainer'
import { Paper, Table, TableRow, TableCell, TableHead, TableBody, styled } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'

const daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

const RowLabelCell = ({children}) => <TableCell sx={{ borderRight: 1, borderColor: '#cccc' }} align='center' width={"8%"}>{children}</TableCell>

const TableComponent = ({timeSlots}) => {    
    return (
        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <RowLabelCell>-</RowLabelCell>
                            {daysOfWeek.map((day, index) => (
                                <TableCell key={day} align="center" sx={{borderRight: index+1 < daysOfWeek.length ? 1 : 0, borderColor: '#cccc'}}>{day}</TableCell>
                            ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                {Array(6).fill().map((_, rowIndex) => (
                    <TableRow key={`row-${rowIndex}`}>
                        <RowLabelCell>
                            {timeSlots != null && `${timeSlots[rowIndex].startTime} - ${timeSlots[rowIndex].endTime}`}
                        </RowLabelCell>

                        {daysOfWeek.map((day, colIndex) => (
                            <TableCell key={`cell-${rowIndex}-${colIndex}`} 
                                align="center"
                                sx={{ borderRight: colIndex<6 ? 1 : 0, borderColor: '#cccc' }}
                            >
                            <AddIcon />
                            </TableCell>
                        ))}
                    </TableRow>
                ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default TableComponent
