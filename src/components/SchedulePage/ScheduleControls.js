import React, { useEffect, useState } from 'react'
import { Container, Typography, Checkbox, Button, FormControlLabel, Box } from '@mui/material'
import TextField from '@mui/material/TextField'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import { useSearchParams } from 'react-router-dom'
import axios from '../../api/axios'
import useAlerts from '../../hooks/useAlerts'
import { getMonday, getSaturday, addDays } from '../../logic/scheduleUtils'

const ScheduleControls = ({dates, nameOfEntity, entityName, onQuery = () => {}}) => {
  const [params, setSearchParams] = useSearchParams()
  const [reservations, setReservations] = useState(false)

  const updateDateParams = (start, end) => {
    let newParams = {}
    for(let p of params.keys()){
        newParams[p] = params.get(p)
    }
    setSearchParams({...newParams, 
        'week-start-date': `${start.getFullYear()}-${start.getMonth()+1}-${start.getDate()}`,
        'week-end-date': `${end.getFullYear()}-${end.getMonth()+1}-${end.getDate()}`
    })
  }

  const handleNext = () => {
    let monday = getMonday(addDays(dates.endDate, 7))
    //let options = { day: 'numeric', month: 'numeric', 'year': 'numeric' }
    let saturday = getSaturday(monday)
    updateDateParams(monday, saturday)
  }

  const handlePrev = () => {
    let monday = getMonday(addDays(dates.startDate, -7))
    let saturday = getSaturday(monday)
    updateDateParams(monday, saturday)
  }

  const handleReservationsChange = () => {
    setReservations(prev => !prev)
  }

  return (
    <Container>
        <Box>
            <Typography variant="h6" gutterBottom>
                Schedule for {nameOfEntity} {entityName}
            </Typography>
            <Typography variant="body2" color="textSecondary">
                Schedule filters
            </Typography>
        </Box>
        <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Button startIcon={<ArrowBackIcon />} 
                variant="outlined" 
                color="primary" 
                sx={{ mr: 1 }}
                onClick={handlePrev}
            >
                Previous
            </Button>
            <Typography variant="subtitle1">{dates?.startDate?.toLocaleDateString()} - {dates?.endDate?.toLocaleDateString()}</Typography>
            <Button endIcon={<ArrowForwardIcon />} 
                variant="outlined" 
                color="primary" 
                sx={{ mx: 1 }}
                onClick={handleNext}
            >
                Next
            </Button>
            <TextField disabled
                select
                label={nameOfEntity}
                variant="outlined"
                SelectProps={{
                    native: true,
                }}
            sx={{ mx: 1, width: '300px' }}
            >
                <option value="entity1">{nameOfEntity}</option>
                {/* <option value="entity2">Entity 2</option>
                <option value="entity3">Entity 3</option> */}
            </TextField>
            <FormControlLabel sx={{ml: 'auto'}} control={<Checkbox value={reservations} color="primary" />} label="Include Reservations" onChange={handleReservationsChange}/>
            <Button variant="outlined" color="primary" onChange={onQuery}>
                Apply
            </Button>
        </Box>
    </Container>
  );
};

export default ScheduleControls
