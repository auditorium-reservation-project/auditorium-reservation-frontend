import React, { useMemo } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/system';

const colorMappings = {
    'Lecture': '#cf1322',
    'LectureBg': '#fff1f0',
    'Seminar': '#d46b08',
    'SeminarBg': '#fff7e6',
    'Practicum': '#1d39c4',
    'PracticumBg': '#f0f5ff',
    'Laboratory': '#096dd9',
    'LaboratoryBg': '#e6f7ff',
    'Control': '#531dab',
    'ControlBg': '#f9f0ff',
    'Default': '#cccccc',
    'DefaultBg': '#fafafa',
    get: function (val, tinted = false) {        
        if(tinted){
            return this[val+'Bg'] ?? this['DefaultBg']
        }
        return this[val] ?? this['Default']
    } 
}

function ClassCard({_class}) {
    const mainColor = useMemo(() => colorMappings.get(_class?.classType), [_class])

    return (
        <Card sx={{ 
            border: `1px solid ${mainColor}`, 
            backgroundColor: colorMappings.get(_class?.classType, true), 
            m: 0,
            p: 0 
        }} elevation={0}>
            <CardContent sx={{m:0, p:1, paddingBottom:0}}>
                <Typography variant="subtitle2" sx={{ color: mainColor }}>
                    {_class?.name ?? _class?.title}
                </Typography>
                <Typography variant="caption" sx={{ color: mainColor }}  gutterBottom={false}>
                <pre>{_class?.auditoriumName}
                    {"\n"+_class?.studentGroupNames?.join(' ')}</pre>
                </Typography>
            </CardContent>
        </Card>
    );
};

export default ClassCard
