import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/system';

const colorMappings = {
    'Lecture': '#cf1322',
    'LectureBg': '#fff1f0',
    'Seminar': '#d46b08',
    'SeminarBg': '#fff7e6',
    'Practicum': '#1d39c4',
    'PracticumBg': '#f0f5ff',
    'Laboratory': '#096dd9',
    'LaboratoryBg': '#e6f7ff',
    'Control': '#531dab',
    'ControlBg': '#f9f0ff',
    'Default': '#cccccc',
    'DefaultBg': '#fafafa',
    get: function (val, tinted = false) {        
        if(tinted){
            return this[val+'Bg'] ?? this['DefaultBg']
        }
        return this[val] ?? this['Default']
    } 
}

function ClassCard({_class}) {
    return (
        <Card sx={{ 
            border: `1px solid ${colorMappings.get(_class?.classType)}`, 
            backgroundColor: colorMappings.get(_class?.classType, true), 
            m: 3 
        }}>
            <CardContent>
                <Typography variant="h6" color="primary" gutterBottom>
                    {_class?.Name}
                </Typography>
                <Typography variant="subtitle1" color="primary">
                    {_class?.auditoriumName}
                </Typography>
                <Typography variant="subtitle1" color="primary">
                    {_class?.studentGroupNames?.join(' ')}
                </Typography>
            </CardContent>
        </Card>
    );
};

export default ClassCard
