import React, { useEffect } from 'react'
import TableContainer from '@mui/material/TableContainer'
import { Paper, Table, TableRow, TableCell, TableHead, TableBody, styled, Typography } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import ClassCard from './ClassCard'

const daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

function RowLabelCell({children, sxProps}) {
    return(
        <TableCell sx={{ borderRight: 1, ...sxProps, borderColor: '#cccc' }} align='center' width={"8%"}>
            {children}
        </TableCell>
    )
}

function AddReservationButton({onClick = () => {}}){
    return(
        <AddIcon sx={{ cursor: 'cell' }} onClick={onClick}/>
    )
}

function TableComponent ({timeSlots, data, resData, onAdd = (dayIndex, slotIndex) => {}}) {

    return (
        <TableContainer component={Paper} elevation={0}>
            <Table>
                <TableHead>
                    <TableRow>
                        <RowLabelCell>-</RowLabelCell>
                            {daysOfWeek.map((day, index) => (
                                <TableCell key={day} align="center" sx={{borderRight: index+1 < daysOfWeek.length ? 1 : 0, borderColor: '#cccc'}}>
                                    <Typography>
                                        {data?.[index]?.date?.toLocaleDateString(undefined, { weekday: 'long' })}
                                    </Typography>
                                    {data?.[index]?.date.toLocaleDateString(undefined, {month: 'long', day: 'numeric'})}
                                </TableCell>
                            ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                {(timeSlots ?? Array(6).fill()).map((slot, slotIndex) => (
                    <TableRow key={`row-${slotIndex}`}>
                        <RowLabelCell sxProps={{ borderBottom: slotIndex + 1 < timeSlots?.length ? 1 : 0 }}>
                            {timeSlots != null && `${timeSlots[slotIndex].startTime} - ${timeSlots[slotIndex].endTime}`}
                        </RowLabelCell>

                        {daysOfWeek.map((day, colIndex) => (
                            <TableCell key={`cell-${slotIndex}-${colIndex}`} 
                                align="center"
                                sx={{ 
                                    borderRight: colIndex < 5 ? 1 : 0,
                                    borderBottom: slotIndex + 1 < timeSlots?.length ? 1 : 0,
                                    borderColor: '#cccc' 
                                }}
                            >
                                {
                                    data?.[colIndex]?.slots[slotIndex] != null && (
                                        data[colIndex].slots[slotIndex].type !== "Empty" ?
                                        <ClassCard _class={data[colIndex].slots[slotIndex]}/>
                                        :
                                            resData?.[colIndex]?.slots[slotIndex]?.type === "Reservation" ?
                                            <ClassCard _class={resData[colIndex].slots[slotIndex]} />
                                                :
                                                <AddReservationButton onClick={() => onAdd(colIndex, slotIndex)} />
                                    ) 
                                }
                            </TableCell>
                        ))}
                    </TableRow>
                ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default TableComponent
