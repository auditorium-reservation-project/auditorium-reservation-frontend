import React, { useState, useEffect } from 'react'
import { Button, Divider, Typography } from '@mui/material'
import TargetChoiceOption from './TargetChoiceOption'
import PeopleIcon from '@mui/icons-material/PeopleAlt'
import OtherSelect from '../Util/OtherSelect'
import axios from '../../api/axios'
import { useNavigate } from 'react-router-dom'

function AuditoriaChoice({handleProceed = () => {}}) {
    const [showAuditoriaSection, setShowAuditoriaSection] = useState(false)
    const [auditoria, setAuditoria] = useState(null)
    const [selAuditorium, setSelAuditorium] = useState(null)
    const [buildings, setBuildings] = useState(null)
    const [selBuilding, setSelBuilding] = useState(null)
    const navigate = useNavigate()

    useState(() => {
        axios.get(`/buildings?detailed=true`)
        .then(res => {
            setBuildings(res?.data)
        }).catch(err => console.warn(err))
    }, [])

    const onFacultyChange = newVal => {
        setAuditoria(newVal?.auditoria ?? null)
        setSelBuilding(newVal)
    }

    const onGroupChange = newVal => {
        setSelAuditorium(newVal)
    }

    

    return (
        <TargetChoiceOption 
            title="Auditoria"
            icon={<PeopleIcon />} 
            showContent={showAuditoriaSection} 
            onClick={() => setShowAuditoriaSection(prev => !prev)}  
        >
            <Typography gutterBottom>
                Show schedule for auditoria
            </Typography>

            {buildings != null ?
                <OtherSelect label="Pick Building" 
                    items={buildings} 
                    localizations={buildings.map(x => x.name)} 
                    onChange={onFacultyChange}
                />
                : <Typography gutterBottom>Loading Buildings</Typography>
            }  
            <Divider sx={{ my: 2 }} />
            {auditoria != null ?
                <OtherSelect label="Pick Auditorium" 
                    items={auditoria}
                    localizations={auditoria.map(x => x.name)} 
                    onChange={onGroupChange}
                />
                : <Typography gutterBottom>Loading Auditoria</Typography>
            }  

            {selAuditorium != null &&
                <Button variant="outlined" 
                    color="primary" 
                    onClick={() => handleProceed(selAuditorium?.id)} 
                    sx={{ mt: 2 }}
                >
                    Show Schedule
                </Button>
            }
        </TargetChoiceOption>
    )
}

export default AuditoriaChoice