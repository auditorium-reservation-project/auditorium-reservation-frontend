import React, { useState, useEffect } from 'react'
import { Button, Divider, Typography } from '@mui/material'
import TargetChoiceOption from './TargetChoiceOption'
import PeopleIcon from '@mui/icons-material/PeopleAlt'
import OtherSelect from '../../components/Util/OtherSelect'
import axios from '../../api/axios'
import { useNavigate } from 'react-router-dom'

function StudentGroupChoice() {
    const [showGroupSection, setShowGroupSection] = useState(false)
    const [groups, setGroups] = useState(null)
    const [selGroup, setSelGroup] = useState(null)
    const [faculties, setFaculties] = useState(null)
    const [selFaculty, setSelFaculty] = useState(null)
    const navigate = useNavigate()

    useState(() => {
        axios.get(`/faculties?includeGroups=true`)
        .then(res => {
            console.warn(res)
            setFaculties(res?.data)
        }).catch(err => console.warn(err))
    }, [])

    const onFacultyChange = newVal => {
        setGroups(newVal?.groups ?? null)
        setSelFaculty(newVal)
    }

    const onGroupChange = newVal => {
        setSelGroup(newVal)
    }

    const handleProceed = () => {
        navigate(`/schedule?target=StudentGroup&targetId=${selGroup?.id}`)
    }

    return (
        <TargetChoiceOption 
            title="Student Groups"
            icon={<PeopleIcon />} 
            showContent={showGroupSection} 
            onClick={() => setShowGroupSection(prev => !prev)}  
        >
            <Typography gutterBottom>
                Show schedule for student groups
            </Typography>

            {faculties != null ?
                <OtherSelect label="Pick Faculty" 
                    items={faculties} 
                    localizations={faculties.map(x => x.name)} 
                    onChange={onFacultyChange}
                />
                : <Typography gutterBottom>Loading Faculties</Typography>
            }  
            <Divider sx={{ my: 2 }} />
            {groups != null ?
                <OtherSelect label="Pick Student Group" 
                    items={groups}
                    localizations={groups.map(x => x.name)} 
                    onChange={onGroupChange}
                />
                : <Typography gutterBottom>Loading Groups</Typography>
            }  

            {selGroup != null &&
                <Button variant="outlined" 
                    color="primary" 
                    onClick={handleProceed} 
                    sx={{ mt: 2 }}
                >
                    Show Schedule
                </Button>
            }
        </TargetChoiceOption>
    )
}

export default StudentGroupChoice