import React, { useState, useEffect } from 'react'
import { Button, Divider, Typography } from '@mui/material'
import TargetChoiceOption from './TargetChoiceOption'
import PeopleIcon from '@mui/icons-material/PeopleAlt'
import OtherSelect from '../Util/OtherSelect'
import axios from '../../api/axios'
import { useNavigate } from 'react-router-dom'

function StudentGroupChoice() {
    const [showGroupSection, setShowGroupSection] = useState(false)
    const [professors, setProfessors] = useState(null)
    const [selProfessor, setSelProfessor] = useState(null)
    const navigate = useNavigate()

    useState(() => {
        axios.get(`/professors`)
        .then(res => {
            console.warn(res)
            setProfessors(res?.data)
        }).catch(err => console.warn(err))
    }, [])

    const onProfessorChange = newVal => {
        setSelProfessor(newVal)
    }

    const handleProceed = () => {
        navigate(`/schedule?target=Professor&targetId=${selProfessor?.id}`)
    }

    return (
        <TargetChoiceOption 
            title="Professors"
            icon={<PeopleIcon />} 
            showContent={showGroupSection} 
            onClick={() => setShowGroupSection(prev => !prev)}  
        >
            <Typography gutterBottom>
                Show schedule for professors
            </Typography>

            {professors != null ?
                <OtherSelect label="Pick Professor" 
                    items={professors.map(x => x.fullName)}
                    initializeWith={professors?.[0]?.fullName}
                    localizations={professors.map(x => x.fullName)} 
                    onChange={onProfessorChange}
                />
                : <Typography gutterBottom>Loading Professors</Typography>
            } 
            {selProfessor != null &&
                <Button variant="outlined" 
                    color="primary" 
                    onClick={handleProceed} 
                    sx={{ mt: 2 }}
                >
                    Show Schedule
                </Button>
            }
        </TargetChoiceOption>
    )
}

export default StudentGroupChoice