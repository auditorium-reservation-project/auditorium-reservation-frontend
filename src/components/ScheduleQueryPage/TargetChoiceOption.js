import React from 'react';
import { Card, CardActionArea, CardContent, Typography } from '@mui/material';

function TargetChoiceOption({ children, title, icon, showContent, onClick }) {
    return (
        <Card variant='outlined' sx={{ my: 1 }}>
            <CardActionArea onClick={onClick} sx={{ display: 'inline-flex', justifyContent: 'center' }}>
                {icon}
                <Typography gutterBottom variant="h5" component="div" sx={{ m: 1 }}>
                    {title}
                </Typography>
            </CardActionArea>
            {showContent === true &&
                <CardContent>
                    {children}
                </CardContent>}
        </Card>
    )
}

export default TargetChoiceOption