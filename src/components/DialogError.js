import React from 'react'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'

function DialogError({ open, onClose, error}) {
    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Error!</DialogTitle>
            <DialogContent>
                <p>{JSON.stringify(error)}</p>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>Close</Button>
            </DialogActions>
        </Dialog>
    )
}

export default DialogError