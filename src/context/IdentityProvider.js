import { createContext, useEffect, useState } from 'react'
import axios from '../api/axios'

const LOGOUT_URL = "/auth/logout"
const LOGIN_URL = "/auth/login"
const USER_SELF_URL = "/users/self"

const IdentityContext = createContext({
    user: {}, 
    login: async (payload) => {}, 
    logout: async () => {}, 
    refresh: async () => {}
})

const IdentityProvider = ({children}) => {
    const [user, setUser] = useState(null)

    const logout = async () => {
        setUser(null)
        try {
            await axios.post(LOGOUT_URL)
        } catch(error){
            console.error(error)
        }
    }

    const login = async (payload) => {
        await axios.post(LOGIN_URL, payload)
        await refreshUser()
    }

    const refreshUser = async () => {
        const userResponse = await axios.get(USER_SELF_URL)
        if(userResponse?.status === 200){
            setUser({...userResponse?.data})
        }
    }

    useEffect(() => {
        console.log("Identity mount")
        refreshUser().catch(err => { console.log("refresh on mount failed") })
    }, [])
    return (
        <IdentityContext.Provider value={{ user, login, logout, refresh: refreshUser }}>
            {children}
        </IdentityContext.Provider>
    )   
}

export default IdentityContext
export { IdentityProvider }