import React, { createContext, useEffect, useState } from 'react'
import { Dialog, DialogTitle, DialogActions, DialogContent, Button } from '@mui/material'

const AlertContext = createContext((content, title = "Error") => {})

const AlertHandler = ({children}) => {    
    const [alert, setAlert] = useState(null)
    const [open, setOpen] = useState(false)

    const handleClose = () => {
        setOpen(false)
        setAlert(null)
    }

    useEffect(() => {
        if(alert){
            setOpen(true)
        }
    }, [alert])

    const callAlert = (_content, _title = "Error") =>{
        setAlert({content: _content, title: _title})
    }

    return (
        <AlertContext.Provider value={callAlert}>
            {children}
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>{alert?.title}</DialogTitle>
                <DialogContent>
                    <pre>{alert?.content}</pre>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>OK</Button>
                </DialogActions>
            </Dialog>
        </AlertContext.Provider>
    )
}

export default AlertContext
export { AlertHandler }