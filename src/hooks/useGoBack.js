import { useLocation, useNavigate } from 'react-router-dom'


const useGoBack = () => {
    const location = useLocation()
    const navigate = useNavigate()
    const prevLocation = location?.state?.from?.pathname || "/"

    const navigateBack = () => {
        navigate(prevLocation, { replace: true })
    }

    return navigateBack
}

export default useGoBack