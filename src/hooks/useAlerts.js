import { useContext } from 'react'
import AlertContext from '../context/AlertHandler'

const useAlerts = () => {
    return useContext(AlertContext)
}

export default useAlerts