import React, { useState } from 'react'
import { InputAdornment } from '@mui/material'
import { Visibility as VisibilityIcon, VisibilityOff as VisibilityOffIcon } from '@mui/icons-material'

// Returns props that need to be passed into the input field and a setter for show state (true/false)

// pasing props: 
// const [props, setShowPassword] = usePasswordHider()
// ...
// <InputField required label="mylabel" {...props} />

// You can use same props in multiple Input components to sync their hide/show
const usePasswordHider = () => {
    const [showPassword, setShowPassword] = useState(false)

    const handleShowPassword = () => {
        setShowPassword(prev => !prev)
    }

    const showPasswordProps = {
        type: showPassword ? 'text' : 'password',
        InputProps:{
            endAdornment: (
                <InputAdornment position="end" sx={{cursor: 'pointer'}}> 
                    {showPassword ? 
                        <VisibilityIcon onClick={handleShowPassword}/>  :
                        <VisibilityOffIcon onClick={handleShowPassword} />
                    }
                </InputAdornment>
            )
        }
    }
    return [showPasswordProps, setShowPassword]
}

export default usePasswordHider