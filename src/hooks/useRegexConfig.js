import { useState, useEffect, useMemo } from 'react'
import axios from '../api/axios'

const useRegexConfig = () => {
    const dict = {}
    dict['firstName'] = /^[A-Za-zА-Яа-я\-]{1,50}$/
    dict['lastName'] = /^[A-Za-zА-Яа-я\-]{1,50}$|(^$)/
    dict['middleName'] = /^[A-Za-zА-Яа-я\-]{1,50}$|(^$)/
    dict['password'] = /^([a-zA-Z0-9@*#~!$]{8,20})$/
    dict['email'] = /^([a-zA-Z0-9_\-.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
    dict[null] = /.*/
    return dict
}

export default useRegexConfig