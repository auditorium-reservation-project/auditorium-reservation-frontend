import axios from '../api/axios'
import useIdentity from './useIdentity'

const REGISTER_URL = "/auth/register"
const USER_SELF_URL = "/users/self"

const useRegister = () => {
    const { refresh } = useIdentity()

    const register = async (payload) => {
        try {
            await axios.post(REGISTER_URL, payload)
            //refresh()
        } catch(error){
            console.error(error)
            return Promise.reject(error)
        }
    }

    return register
}

export default useRegister