import { red } from '@mui/material/colors'
import { createTheme } from '@mui/material/styles'

// A custom theme for this app
const theme = createTheme({
  palette: {
    primary: {
      main: '#2f80ed',
      dark: '#71717a',
    },
    secondary: {
      // main: '#19857b',
      main: '#f1f1f1',
    },
    error: {
      main: "#bd0000",
    },
    selected: {
      main: '#71717a'   
    },
    btnSecondary: {
      main: '#bdbdbd'
    },
    btnOutlined: {
      main: '#71717a',
      dark: '#ff0033',
      light: '#0033ff'
    },
    success: {
      main: "#219653"
    },
    warning: {
      main: "#b45309"
    },
    info: {
      main: "#0369a1"
    },
    action: {
      main: "#6d28d9"
    }
  },
  // typography: {
  //   fontFamily: [
  //     'Inter',
  //     'monospace'
  //   ].join(','),
  //   fontStyle: 'normal'
  // },
  text: {
    primary: {
      main: '#4f4f4f',
      light: '#ffffff'
    },
    secondary: {
      main: '#333333'
    }
  },
  slots: {
    practicum: '#0EA5E9',
    seminar: '#F59E0B',
    lecture: '#10B981',
    alt: '#8B5CF6'
  }
})

export default theme
