import React, { useEffect, useState, useRef, useMemo } from 'react'
import useAlerts from '../hooks/useAlerts'
import { Container, Grid, IconButton, Typography, Dialog, DialogTitle, DialogContent, DialogActions, Button, Card } from '@mui/material'
import EditIcon from '@mui/icons-material/Edit'
import ProfileAttribute from '../components/ProfilePage/ProfileAttribute'
import AttributeSelect from '../components/Util/AttributeSelect'
import useRegexConfig from '../hooks/useRegexConfig'
import { useNavigate } from 'react-router-dom'
import Roles from '../api/Roles'
import axios from '../api/axios'
import { Add } from '@mui/icons-material'
import { roleColors } from './ProfilePage'
import OtherSelect from '../components/Util/OtherSelect'
import useIdentity from '../hooks/useIdentity'


function RoleApplicationsPage() {
    const [selected, setSelected] = useState(Roles.staff)
    const [isProcessing, setIsProcessing] = useState(false)
    const alert = useAlerts()
    const navigate = useNavigate()
    const {user} = useIdentity()
    const availableRoles = useMemo(() => Object.values(Roles).filter(x => user?.roles?.includes(x) === false && x!=="Default"), [user])

    const handleApply = () => {
        let role = selected
        setIsProcessing(true)
        axios.post('/applications/roles?role=' + role)
        .then(res => {
            alert("Applied for role " + role, "Success")
            setIsProcessing(false)
        })
        .catch(err => {
            alert("Unable to apply for role")
            setIsProcessing(false)
        })
    }

    return (
    <>
        <Container maxWidth="md" sx={{ mt: 12 }}>
            <Typography variant="h4">
                Apply for role
            </Typography>
            <OtherSelect initializeWith={selected} items={availableRoles} localizations={availableRoles} label="Select Desired Role"  onChange={x => setSelected(x)}/>
            <Button variant='outlined' onClick={handleApply} disabled={isProcessing}>Apply</Button>
        </Container>
    </>
    )
}

export default RoleApplicationsPage