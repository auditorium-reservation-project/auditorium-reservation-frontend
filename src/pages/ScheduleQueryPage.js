import React, { useState } from 'react'
import { Box, Container, Typography, CardHeader } from '@mui/material'
import PersonIcon from '@mui/icons-material/Person';
import MeetingRoomIcon from '@mui/icons-material/MeetingRoom';
import { useNavigate } from 'react-router-dom';
import TargetChoiceOption from '../components/ScheduleQueryPage/TargetChoiceOption';
import StudentGroupChoice from '../components/ScheduleQueryPage/StudentGroupChoice';
import ProfessorChoice from '../components/ScheduleQueryPage/ProfessorChoice';
import AuditoriaChoice from '../components/ScheduleQueryPage/AuditoriaChoice';

function ScheduleQueryPage() {
    
    const [showProfessorSection, setShowProfessorSection] = useState(false)
    const [showAuditoriaSection, setShowAuditoriaSection] = useState(false)
    const navigate = useNavigate()

    return (
        <Container component="main" maxWidth='md'>
            <Box sx={{ marginTop: 12 }}>
                <StudentGroupChoice />
                <ProfessorChoice />
                <AuditoriaChoice handleProceed={x => navigate(`/schedule?target=Auditorium&targetId=${x}`)}/>
            </Box>
        </Container>
    )
}
export default ScheduleQueryPage