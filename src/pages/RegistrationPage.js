import React, { useEffect, useState } from 'react'
import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import Link from '@mui/material/Link'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import Typography from '@mui/material/Typography'
import Container from '@mui/material/Container'
import theme from '../theme'
import useRegister from '../hooks/useRegister'
import useGoBack from '../hooks/useGoBack'
import useIdentity from '../hooks/useIdentity'
import useRegexConfig from '../hooks/useRegexConfig'
import InputField from '../components/RegistrationPage/InputField'
import usePasswordHider from '../hooks/usePasswordHider'
import useAlerts from '../hooks/useAlerts'
import axios from '../api/axios'

const GET_STUDENT_GROUPS_URL = "/groups"

const RegistrationPage = () => {
    const register = useRegister()
    const goBack = useGoBack()
    const [selGroup, setSelGroup] = useState({})
    const [showPasswordProps, setShowPassword] = usePasswordHider()
    const [groups, setGroups] = useState([])
    const { user } = useIdentity()
    const alert = useAlerts()
    const regex = useRegexConfig()


    const handleSubmit = (event) => {
        
        event.preventDefault()
        const data = new FormData(event.currentTarget)
        const payload = {
            firstName: data.get('firstName'),
            middleName: data.get('middleName'),
            lastName: data.get('lastName'),
            email: data.get('email'),
            groupId: selGroup.toString(),
            password: data.get('password'),
        }
        
        if(payload.middleName?.length <= 0){
            payload.middleName = null
        }
        
        if(payload.lastName?.length <= 0){
            payload.lastName = null
        }

        if(payload.firstName?.length <= 0){
            validationFail("First Name cannot be empty")
        }

        
        if( !regex['firstName']?.test(payload.firstName)
        ||  !regex['middleName']?.test(payload.middleName)
        ||  !regex['lastName']?.test(payload.lastName)
        ||  !regex['email']?.test(payload.email)){
                validationFail("Invalid registration data, please check your credentials")
                return
        }
        
        if(payload.password !== data.get('confirmPassword')){
            validationFail("Provided passwords do not match")
            return
        }
        
        register(payload)
        .then(response => { 
            goBack()
        })
        .catch(error => validationFail(error))
    }

    const validationFail = (error) => {
        alert(error)
        if(error){
            console.warn("Validation error: ", error)
        }
    }
    useEffect(() => {
        if(user !== null){
            goBack()
            return
        }

        axios.get(GET_STUDENT_GROUPS_URL)
        .then(response => {
            const data = response?.data
                ?.map(val => ({name: val.name, id: val.id}))
            setGroups(data)
        })
        .catch(error => console.warn(error))
    }, [goBack, user])

    return (
    <Container component="main" maxWidth="xs">
        <Box
        sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}
        >
        <Avatar sx={{ m: 1, bgcolor: theme.palette.secondary.main }}>
            <LockOutlinedIcon sx={{color: theme.palette.primary.dark}}/>
        </Avatar>
        <Typography component="h1" variant="h5">
            Registration
        </Typography>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <InputField name="firstName" label="First Name" autoComplete="given-name" regex={regex['firstName']} props={{autoFocus: true}}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <InputField name="middleName" label="Middle Name" autoComplete="middle-name" regex={regex['middleName']} />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <InputField name="lastName" label="Last Name" autoComplete="family-name" regex={regex['lastName']} />
                </Grid>
                
                <Grid item xs={12}>
                    <InputField name="email" label="Email" autoComplete="email" hint={"Invalid Email"} regex={regex['email']} />
                </Grid>
                <Grid item xs={12}>
                    <InputField name="password" label="Password" autoComplete="new-password" hint={"A password must be 8 to 20 symbols"} regex={regex['password']} props={showPasswordProps} />
                </Grid>
                <Grid item xs={12}>
                    <InputField name="confirmPassword" label="Confirm Password" autoComplete="confirm-password new-password" hint="" regex={regex['password']} props={showPasswordProps} />
                </Grid>
                <Grid item xs={12}>
                    {/* <FormControlLabel
                        control={<Checkbox value="allowExtraEmails" color="primary" />}
                        label={<Typography sx={{fontSize:'0.8rem'}}>{Localization.confirmSpam}</Typography>}
                    /> */}
                </Grid>
            </Grid>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                >
                    Registration
            </Button>
            <Grid container justifyContent="flex-end">
                <Grid item>
                    <Link href="/login" variant="body2">
                        Register to login
                    </Link>
                </Grid>
            </Grid>
        </Box>
    </Box>
    </Container>
  )
}

export default RegistrationPage