import React, { useState } from 'react'
import { Box, Container, Typography, CardHeader, Button, CircularProgress } from '@mui/material'
import axios from '../api/axios'
import useAlerts from '../hooks/useAlerts'

function ScheduleAdminPage() {
    const alert = useAlerts()
    const [isInAction, setIsInAciton] = useState(false)

    const handleSync = () => {
        if(isInAction) return
        setIsInAciton(true)
        axios.post('/classes/sync')
        .then(res => alert("Successfuly synched", ""))
        .catch(err => {alert(err?.message); console.log(err)})
        .finally(() => setIsInAciton(false))
    }

    const handleClear = () => {
        if(isInAction) return
        setIsInAciton(true)
        axios.delete('/classes/sync')
        .then(res => alert("Successfuly synched", "Success"))
        .catch(err => {alert(err?.message); console.log(err)})
        .finally(() => setIsInAciton(false))
    }
    return (
        <Container component="main" maxWidth='md'>
            <Box sx={{ marginTop: 5 }}>
                <Typography>Schedule manual synchronization</Typography>
                <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                    { isInAction && <CircularProgress />}
                </Box>
                <Button onClick={handleSync} disabled={isInAction}>Sync Schedule</Button>
                <Button onClick={handleClear} disabled={isInAction}>Clear Sync Data</Button>
            </Box>
        </Container>
    )
}
export default ScheduleAdminPage