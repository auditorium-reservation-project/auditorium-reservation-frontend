import React from 'react'
import { Box, Container } from '@mui/material'
import { useLocation } from 'react-router-dom'
import LinkCard from '../components/MainPage/LinkCard'
import RequireRoles from '../components/Util/RequireRoles'
import Roles from '../api/Roles'


function MainPage() {
    const location = useLocation()

    return (
        <Container component="main" maxWidth='md'>
            <Box sx={{ marginTop: 12 }}>
                <LinkCard href="/schedule"
                    title={"View schedule"} 
                    desc={"View current schedule"}
                    location={location}
                />

                <RequireRoles allowedRoles={[Roles.staff, Roles.admin]}>
                    <LinkCard href="/reservations/consider"
                        title={"Application management"}
                        desc={"Consider role and reservation applications for the schedule"}
                        location={location}
                    />
                </RequireRoles>

            </Box>
        </Container>
    )
}
export default MainPage