import React, {useState, useEffect} from 'react'
import { Button, Box, Container, Dialog, DialogActions, DialogTitle, DialogContent, Grid, TextField, Typography, FormControlLabel, Checkbox } from '@mui/material'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom'
import RequireRoles from '../components/Util/RequireRoles'
import Roles from '../api/Roles'
import TestGrid from '../components/TestGrid'
import ReservationsTable from '../components/SchedulePage/ReservationsTable'
import ReservationScheduleControls from '../components/SchedulePage/ReservationScheduleControls'
import useAlerts from '../hooks/useAlerts'
import axios from '../api/axios'
import { getMonday, getSaturday } from '../logic/scheduleUtils'

function ScheduleReservationsPage() {
    const navigate = useNavigate()
    const [timeSlots, setTimeSlots] = useState()
    const [data, setData] = useState([])
    const [resData, setResData] = useState([])
    const [dates, setDates] = useState({})
    const [auditorium, setAuditorium] = useState({})
    const [addItemIdx, setAddItemIdx] = useState(null)
    const [addDialogOpen, setAddDialogOpen] = useState(false)
    const [notify, setNotify] = useState(false)
    const [params, setSearchParams] = useSearchParams()
    const alert = useAlerts()


    const queryData = () => {
        let auditoriumId = params.get('targetId')
        let weekStartDate = params.get('week-start-date')
        let weekEndDate = params.get('week-end-date')

        if(auditoriumId == null){
            navigate('/reservations/query')
            return
        }

        if(weekStartDate == null){
            weekStartDate = getMonday(weekEndDate ?? new Date())
        } else { 
            weekStartDate = new Date(weekStartDate)
        }
        if(weekEndDate == null){
            weekEndDate = getSaturday(weekStartDate)
        }else {
            weekEndDate = new Date(weekEndDate)
        }

        axios.get(`/reservations/auditoria/${auditoriumId}?StartDate=${weekStartDate.toDateString()}&EndDate=${weekEndDate.toDateString()}`)
        .then(res => {
            res.data.days = res.data.days.map(x => ({...x, 
                date: new Date(x.date),
            }))
            console.log(res?.data)
            setResData(res?.data)
        })
        .catch(err => {
            console.warn(err)
            alert("Failed to load reservations data")
        })

        axios.get(`/classes?Type=Auditorium&RequestForEntityId=${auditoriumId}&StartDate=${weekStartDate.toDateString()}&EndDate=${weekEndDate.toDateString()}&includeReservation=${false}`)
        .then(res => {
            if(res?.data?.days == null){
                alert("Failed loading schedule data")
                navigate('/reservations/query')
                return
            }
            setDates({startDate: weekStartDate, endDate: weekEndDate})
            res.data.days = res.data.days.map(x => ({...x, 
                date: new Date(x.date),
            }))
            setData(res?.data)
        })
        .catch(err => console.log(err))


        axios.get(`/auditoria/${auditoriumId}`)
        .then(res => setAuditorium(res?.data))
        .catch(err => navigate('/reservations/query'))
    }

    useEffect(() => {
        queryData()
    }, [])

    useEffect(() => {
        axios.get("/classes/time-slots")
        .then(res => {
        setTimeSlots(res?.data?.map(x => ({
            ...x, 
            startTime: x.startTime.slice(0,5), 
            endTime: x.endTime.slice(0,5)
        })))
        })
        .catch(err => alert("failed to load time slots data"))
    }, [setTimeSlots, alert])
    
    const handleAddNew = (dayIndex, slotIndex) => {
        setAddItemIdx({day: dayIndex, slot: slotIndex})
        console.log(data)
        setAddDialogOpen(true)
    }

    const handleAddApply = async (e) => {
        e.preventDefault()
        let form = new FormData(e.currentTarget)
        let date = new Date(data.days[addItemIdx.day].date)
        const payload = {
            auditoriumId: auditorium.id,
            date: `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate() > 9 ? date.getDate() : "0" + date.getDate()}`,
            timeSlotIndex: data.days[addItemIdx.day].slots[addItemIdx.slot].slotIndex,
            name: form.get('title'),
            details: form.get('description'),
            notifyByEmail: notify
        }
        axios.post('/reservations', payload)
        .then(res => {
            alert("Reservation created successfuly", "Success")
            queryData()
        })
        .catch(err => {
            console.warn(err)
            alert("Reservation creation failed: verify input data")
        })
        setAddDialogOpen(false)
        setAddItemIdx(null)
    }
    
    const handleAddClose = () => {
        setAddDialogOpen(false)
        setAddItemIdx(null)
    }

    return (
        <Container component="main" maxWidth='xl'>
            <ReservationScheduleControls dates={dates} nameOfEntity={"Auditorium"} entityName={auditorium?.name}/>
            <Box sx={{ marginTop: 2, display: 'flex', justifyContent: 'space-between' }}>
                <ReservationsTable timeSlots={timeSlots} data={data?.days} resData={resData} onAdd={handleAddNew}/>
            </Box>

            { addItemIdx != null &&
            <Dialog open={addDialogOpen} onClose={handleAddClose} fullWidth maxWidth="sm">
                <DialogTitle>{`Add new reservation for ${auditorium?.name}`}</DialogTitle>
                <DialogContent>
                    <Grid container columns={12} rowSpacing={3.5} sx={{ flexGrow: 1 }}>
                    <Box component="form" onSubmit={handleAddApply} noValidate sx={{ mt: 1 }}>                        
                        <TextField
                            sx={{mt: 5}}
                            required
                            fullWidth
                            id="title"
                            label="Title"
                            name="title"
                            autoComplete="title"
                            autoFocus                            
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="description"
                            label="Description"
                            type="description"
                            id="description"
                            autoComplete="description"
                        />
                        <FormControlLabel sx={{ml: 'auto'}} id="email-notification" control={<Checkbox value={notify} color="primary"/>} label="Notify by Email" onChange={x => setNotify(prev => !prev)}/>
                        <Typography variant='subtitle2'>
                            {data.days[addItemIdx.day].date.toDateString()}
                        </Typography>
                        <Button type="submit" variant='outlined'>Apply</Button>
                    </Box>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleAddClose}>Cancel</Button>                    
                </DialogActions>
            </Dialog>
            }
        </Container>
    )
}

export default ScheduleReservationsPage