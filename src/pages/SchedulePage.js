import React, {useState, useEffect} from 'react'
import { Box, Container } from '@mui/material'
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom'
import RequireRoles from '../components/Util/RequireRoles'
import Roles from '../api/Roles'
import TestGrid from '../components/TestGrid'
import TestTable from '../components/SchedulePage/ViewTable'
import ScheduleControls from '../components/SchedulePage/ScheduleControls'
import useAlerts from '../hooks/useAlerts'
import axios from '../api/axios'
import { getMonday, getSaturday } from '../logic/scheduleUtils'


function SchedulePage() {
    const navigate = useNavigate()
    const [timeSlots, setTimeSlots] = useState()
    const [data, setData] = useState([])// Schedule table by columns
    const [dates, setDates] = useState({})
    const [params, setSearchParams] = useSearchParams()
    const alert = useAlerts()

    const queryData = (includeReservations = false) => {
        let target = params.get('target')
        let targetId = params.get('targetId')
        let weekStartDate = params.get('week-start-date')
        let weekEndDate = params.get('week-end-date')
        console.log(new Date(weekStartDate))
        if(target == null || targetId == null){
            navigate('/schedule/query')
            return
        }
        if(weekStartDate == null){
            weekStartDate = getMonday(weekEndDate ?? new Date())
        } else { 
            weekStartDate = new Date(weekStartDate)
        }
        if(weekEndDate == null){
            weekEndDate = getSaturday(weekStartDate)
        }else {
            weekEndDate = new Date(weekEndDate)
        }

        axios.get(`/classes?Type=${target}&RequestForEntityId=${targetId}&StartDate=${weekStartDate.toDateString()}&EndDate=${weekEndDate.toDateString()}&includeReservation=${includeReservations}`)
        .then(res => {
            if(res?.data?.days == null){
                alert("Failed loading schedule data")
                navigate('/schedule/query')
                return
            }
            setDates({startDate: weekStartDate, endDate: weekEndDate})
            res.data.days = res.data.days.map(x => ({...x, 
                date: new Date(x.date),
            }))
            setData(res?.data)
        })
        .catch(err => console.log(err))
    }

    useEffect(() => {
        queryData()
        
        //axios.post
    }, [navigate, params])

    useEffect(() => {
        axios.get("/classes/time-slots")
        .then(res => {
        setTimeSlots(res?.data?.map(x => ({
            ...x, 
            startTime: x.startTime.slice(0,5), 
            endTime: x.endTime.slice(0,5)
        })))
        })
        .catch(err => alert("failed to load time slots data"))
    }, [setTimeSlots, alert])

    // useEffect(() => {
    //     console.log(timeSlots)
    // }, [timeSlots])

    return (
        <Container component="main" maxWidth='xl'>
            <ScheduleControls dates={dates} onQuery={queryData} nameOfEntity={params.get('target')}/>
            <Box sx={{ marginTop: 2, display: 'flex', justifyContent: 'space-between' }}>
                {/* <TestGrid /> */}
                <TestTable timeSlots={timeSlots} data={data?.days}/>
            </Box>
        </Container>
    )
}
export default SchedulePage