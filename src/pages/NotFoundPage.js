import React from 'react'
import { Container } from '@mui/material'

function NotFoundPage() {
    return (
        <Container component="main" maxWidth='md' sx={{mt: 2}}>
            404 Not Found!
        </Container>
    )
}

export default NotFoundPage