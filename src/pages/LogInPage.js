import React, { useEffect } from 'react'
import { 
  Avatar, 
  Button, 
  TextField, 
  Link,
  Grid,
  Box,
  Typography,
  Container
} from '@mui/material'
import {LockOutlined as LockOutlinedIcon} from '@mui/icons-material'
import theme from '../theme'
import { NavLink, useLocation, useNavigate } from 'react-router-dom'
import useGoBack from '../hooks/useGoBack'
import usePasswordHider from '../hooks/usePasswordHider'
import useAlerts from '../hooks/useAlerts'
import useIdentity from '../hooks/useIdentity'

function LogInPage() {
    const goBack = useGoBack()
    const { login, user } = useIdentity()
    const location = useLocation()
    const navigate = useNavigate()
    const [passwordFormProps] = usePasswordHider()
    const alert = useAlerts()

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = new FormData(event.currentTarget)
        const loginPayload = {
            email: data.get('login'),
            password: data.get('password')
        }

        if(loginPayload.email === "" || loginPayload.password === ""){
            alert("Login credentials empty")
            return
        }

        try {
            await login(loginPayload)
            navigate('/main')
        } catch(err){
            console.log(err)
            if(err?.response === null){
                alert("No Server Response")
            } else if(err.response?.status === 400) {
                if(err?.response?.data?.errors !== null){
                    handleValidationErrors(err.response.data.errors)
                    return
                }
                alert("Invalid Credentials")
            } else if (err?.response?.status === 401){
                if(typeof err?.response?.data === 'string'){
                    alert(err.response.data)
                    return
                }
                alert("Invalid Credentials")
            }
            else {
                alert("Failed LogIn")
            }
        }
    }

    const handleValidationErrors = (errs) => {
        let message = "Invalid credentials:\n"
        for (const [key, value] of Object.entries(errs)){
            message += `${key}:\n\t` + value.join('\n\t') + "\n"
        }

        alert(message)
    }

    const handleNoAuth = () => {
        navigate("/schedule")
    }  

    useEffect(() => {
        if(user !== null){
            goBack()
        }
    }, [goBack, user])

    return (
        <Container component="main" maxWidth="xs">
            <Box
            sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
            >
            <Avatar sx={{ m: 1, bgcolor: theme.palette.secondary.main }}>
                <LockOutlinedIcon sx={{color: theme.palette.primary.dark}}/>
            </Avatar>
            <Typography component="h1" variant="h5">
                Login
            </Typography>
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="login"
                    label="Email"
                    name="login"
                    autoComplete="login"
                    autoFocus
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    {...passwordFormProps}
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    disableElevation              
                    sx={{ mt: 3, mb: 0.5 }}
                >
                login
                </Button>
                <Button
                    fullWidth
                    variant="outlined"
                    disableElevation
                    color="selected"
                    sx={{ mt: 0, mb: 2 }}
                    onClick={handleNoAuth}
                    component={NavLink}
                    to="/"
                    state={{from: location}}
                >
                {"Proceed without registration"}
                </Button>
                <Grid container>
                <Grid item xs>
                    <Link href="#" variant="body2" onClick={() => navigate("/password-restore")}>
                        {"I Forgot My Password"}
                    </Link>
                </Grid>
                <Grid item>
                    <Link variant="body2" onClick={() => navigate("/registration")}>
                        Register User Account
                    </Link>
                </Grid>
                </Grid>
            </Box>
            </Box>
        </Container>
    )
}

export default LogInPage