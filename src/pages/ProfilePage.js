import React, { useEffect, useState, useRef } from 'react'
import useAlerts from '../hooks/useAlerts'
import { Container, Grid, IconButton, Typography, Dialog, DialogTitle, DialogContent, DialogActions, Button, Card } from '@mui/material'
import EditIcon from '@mui/icons-material/Edit'
import ProfileAttribute from '../components/ProfilePage/ProfileAttribute'
import AttributeSelect from '../components/Util/AttributeSelect'
import useRegexConfig from '../hooks/useRegexConfig'
import { useNavigate } from 'react-router-dom'
import Roles from '../api/Roles'
import axios from '../api/axios'
import { Add } from '@mui/icons-material'

const roleColors = {}
roleColors[Roles.staff] = '#f39c12'
roleColors[Roles.student] = '#27ae60'
roleColors[Roles.professor] = '#3498db'
roleColors[Roles.admin] = '#95a5a6'
roleColors[null] = '#95a5a6'

//gradeBookNumber is currently uneditable since api returns 409 Conflict upon updating it
function ProfilePage() {
    const [data, setData] = useState({})
    const alert = useAlerts()
    const regex = useRegexConfig()
    const navigate = useNavigate()
    const isDataInitialized = useRef(false)
    const [nameEditDialogueOpen, setNameEditDialogueOpen] = useState(false)
    const [oldName, setOldName] = useState({})
    const [newName, setNewName] = useState({})
    const [groups, setClusters] = useState([])

    useEffect(() => {
        axios.get("/users/self")
            .then(response => {
                setData(response?.data)
                isDataInitialized.current = true
            })
            .catch(error => {
                alert("Failed to load profile")
            })

        // axios.get("/users/self/groups")
        //     .then(response => {
        //         setClusters(response?.data.map(val => val.number))
        //     })
        //     .catch(error => {
        //         alert("Failed to load user groups")
        //     })
    }, [])

    const updateUser = (newData) => {
        axios.put(`/users/self`, newData)
            .then(response => {
                console.warn("Successfuly updated")
                setData(newData)
            })
            .catch(error => {
                console.warn(error)            
                alert(JSON.stringify(error?.message))
            })
            .finally(() => /*navigate(0)*/{})
    }

    useEffect(() => {
        setOldName({firstName: data.firstName, lastName: data.lastName, middleName: data.middleName})
    }, [data])

    useEffect(() => {
        if(newName?.firstName || newName?.lastName || newName?.middleName){
            setOldName(prev => ({...prev, ...newName}))
        }
    }, [newName])

    const handleNameEditClose = () => {
        setNameEditDialogueOpen(false)
    }

    const handleNameEditApply = () => {            
        setNameEditDialogueOpen(false)
        updateUser({...data, ...newName})
    }

    return (
    <>
        <Container maxWidth="md" sx={{ mt: 12 }}>
            <Typography variant="h4">
            <Grid container columns={12} sx={{ flexGrow: 1 }} justifyContent="space-between">
                { data?.roles != null &&
                    <Grid item sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'start' }}>
                        {data.roles.map(r => 
                            <Card variant='outlined' 
                                sx={{ display:'inline-flex', 
                                    width: 'fit-content', 
                                    p: 1, 
                                    mr: 0.5,
                                    outlineColor: roleColors[r], 
                                    color: roleColors[r], 
                                    fontSize: "1.2rem" 
                                }}
                                key={r}>
                                {r}
                            </Card>
                        )}
                    </Grid>
                }
                <IconButton onClick={() => navigate('/profile/roles')}>
                    <Add />
                </IconButton>
                <Grid item sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'end' }}>
                    {data?.lastName ?? ""} {data?.firstName ?? ""} {data?.middleName ?? ""}
                    <IconButton onClick={() => setNameEditDialogueOpen(true)}>
                        <EditIcon />
                    </IconButton>
                </Grid>
            </Grid>
            </Typography>
            <Grid container columns={12} rowSpacing={3.5} sx={{ flexGrow: 1, mt:2 }}>
                <ProfileAttribute 
                    title="Email"
                    value={data?.email}
                    regex={regex['email']}
                    hint="user@example.com"
                    onSubmit={(newVal) => updateUser({...data, email: newVal})}
                />
                <ProfileAttribute 
                    title="Student Group"
                    value={data?.group}
                    regex={regex[null]}
                    hint={""}
                    onSubmit={(newVal) => updateUser({...data, gradeBookNumber: newVal})}
                    editable={true}
                />
                <AttributeSelect 
                    title="Student Group"
                    items={groups}
                    value={data?.studentGroup?.name}
                    onSubmit={(newVal) => updateUser({...data, studentGroup: newVal})}
                />
            </Grid>
        </Container>

        <Dialog open={nameEditDialogueOpen} onClose={handleNameEditClose} fullWidth maxWidth="sm">
            <DialogTitle>{`${data?.firstName} ${data?.middleName} ${data?.lastName}`}</DialogTitle>
            <DialogContent>
                <Grid container columns={12} rowSpacing={3.5} sx={{ flexGrow: 1 }}>
                    <ProfileAttribute 
                        title="First Name"
                        value={oldName?.firstName ?? ""}
                        regex={regex['firstName']}
                        onSubmit={(newVal) => setNewName(prev => ({...prev, firstName: newVal}))}
                    />
                    <ProfileAttribute 
                        title="Middle Name"
                        value={oldName?.middleName ?? ""}
                        regex={regex['middleName']}
                        onSubmit={(newVal) => setNewName(prev => ({...prev, middleName: newVal}))}
                    />
                    <ProfileAttribute 
                        title="Last Name"
                        value={oldName?.lastName ?? ""}
                        regex={regex['lastName']}
                        onSubmit={(newVal) => setNewName(prev => ({...prev, lastName: newVal}))}
                    />
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleNameEditClose}>Cancel</Button>
                <Button onClick={handleNameEditApply}>Accept</Button>
            </DialogActions>
        </Dialog>
    </>
    )
}

export default ProfilePage
export {roleColors}