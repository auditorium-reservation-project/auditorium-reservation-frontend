import React from 'react'
import { Container } from '@mui/material'

function NotFoundPage() {
    return (
        <Container component="main" maxWidth='md' sx={{mt: 2}}>
            Unauthorized!
        </Container>
    )
}

export default NotFoundPage