import React, { useEffect, useState, useRef, useMemo } from 'react'
import useAlerts from '../hooks/useAlerts'
import { IconButton, Typography, TableContainer, Paper, Table, Container, TableBody, TableHead, TableRow, TableCell } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import Roles from '../api/Roles'
import axios from '../api/axios'
import OtherSelect from '../components/Util/OtherSelect'
import useIdentity from '../hooks/useIdentity'
import { Cancel, Check } from '@mui/icons-material'


function ReservationApprovalPage() {
    const alert = useAlerts()
    const navigate = useNavigate()
    const [data, setData] = useState(null)


    const queryData = () => {
        axios.get('/reservations/applications')
        .then(res => {
            setData(res?.data)
            console.log(res?.data)
        }).catch(err => {
            alert("Failed to load role applications")
        })
    }
    useEffect(() => {
        queryData()
    }, [])

    const handleApprove = (appl) => {
        axios.post(`/reservations/${appl?.id}/approve`)
        .then(res => {
            alert("Approved", "Success")
        })
        .catch(err => alert("Failed to approve application"))
        .finally(() => queryData())
    }

    const handleReject = (appl) => {
        axios.post(`/reservations/${appl?.id}/reject`)
        .then(res => {
            alert("Rejected", "Success")
        })
        .catch(err => alert("Failed to reject application"))
        .finally(() => queryData())
    }

    return (
        <Container component="main" maxWidth='md'>
        <TableContainer component={Paper} elevation={0}>
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell>
                        <Typography>Reservation requests from users</Typography>
                    </TableCell>
                </TableRow>
                <TableRow>
                    <TableCell>
                        User
                    </TableCell>
                    <TableCell>
                        Auditorium
                    </TableCell>
                    <TableCell>
                        Date
                    </TableCell>
                    <TableCell>
                        Time
                    </TableCell>
                    <TableCell>
                        <IconButton disabled>
                            <Check/>
                        </IconButton>
                    </TableCell>
                    <TableCell>
                        <IconButton disabled>
                            <Cancel />
                        </IconButton>
                    </TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                { data != null && data?.map( x => 
                <TableRow key={x?.id}>
                    <TableCell>
                        {x?.creatorName}
                    </TableCell>
                    <TableCell>
                        {x?.auditorium?.name}
                    </TableCell>
                    <TableCell>
                        {new Date(x?.date).toLocaleDateString(undefined)}
                    </TableCell>
                    <TableCell>
                        {x?.timeSlot?.startTime?.toString("HH:mm")} - {x?.timeSlot.endTime.toString("HH:mm")}
                    </TableCell>
                    <TableCell>
                        <IconButton onClick={() => handleApprove(x)}>
                            <Check/>
                        </IconButton>
                    </TableCell>
                    <TableCell>
                        <IconButton onClick={() => handleReject(x)}>
                            <Cancel />
                        </IconButton>
                    </TableCell>
                </TableRow>
                )}
            </TableBody>
        </Table>
    </TableContainer>
    </Container>
    )
}

export default ReservationApprovalPage